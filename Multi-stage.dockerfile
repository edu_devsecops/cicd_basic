FROM golang:1.19 as build-stage
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY *.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -o /main

FROM build-stage AS run-stage
LABEL authors="dzaitsev"
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=build-stage /main /main
ENTRYPOINT ["/main"]
