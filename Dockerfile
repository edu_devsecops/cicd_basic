FROM alpine
LABEL authors="dzaitsev"
RUN adduser -S -D -H -h /app appuser
USER appuser
WORKDIR /app
COPY ./bin/main ./
COPY ./config ./
ENTRYPOINT ["/app/main"]
